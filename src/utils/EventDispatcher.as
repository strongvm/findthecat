/**
 * Created by vitaliym on 24.11.16.
 */
package utils {
import flash.events.EventDispatcher;

public class EventDispatcher extends flash.events.EventDispatcher {
    private static var _instance:utils.EventDispatcher;
    
    public static const CONFIG_LOADED:String = 'config_loaded';
    
    public function EventDispatcher() {
        _instance = this;
    }
    
    public static function get instance():utils.EventDispatcher{
        _instance ||= new utils.EventDispatcher();
        return _instance;
    }
}
}
