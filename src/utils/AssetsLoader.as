/**
 * Created by vitaliym on 24.11.16.
 */
package utils {
import flash.display.Loader;
import flash.display.LoaderInfo;
import flash.events.Event;
import flash.net.URLRequest;


public class AssetsLoader extends EventDispatcher{

    private var _url:String;
    private var _type:String;
    private var _loader:Loader;
    private var _url_request:URLRequest;
    private var _on_complete:Function;
    private var _position:int;
    private static var _assets:Object = {};
    
    public function AssetsLoader() {
    }

    public function load(strUrl:String, on_complete:Function, type:String = null, position:int = 0):void {
        _url = strUrl;
        _type = type;
        _position = position;
        _loader = new Loader();
        _on_complete = on_complete;
        _loader.contentLoaderInfo.addEventListener(Event.COMPLETE, on_loaded);
        _url_request = new URLRequest('../' + _url);
        _loader.load(_url_request);
    }
    
    private function on_loaded(e:Event):void{
        _assets[_url] = (e.target as LoaderInfo).content;
        _on_complete.call(this, e, _type, _position);
    }
    
    public static function get assets():Object{
        return _assets;
    }
}
}
