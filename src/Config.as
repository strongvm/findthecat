/**
 * Created by vitaliym on 24.11.16.
 */
package {
import flash.events.Event;
import flash.net.URLLoader;
import flash.net.URLRequest;

import utils.EventDispatcher;

public class Config {
    private static var _xml_config:XML;
    private static const XML_PATH:String = "../config.xml";
    private static var _config:Object;
    public function Config() {
    }
    
    public static function load():void{
        _xml_config = new XML();
        _config = {};
        var config_loader:URLLoader = new URLLoader(new URLRequest(XML_PATH));
        config_loader.addEventListener(Event.COMPLETE, xml_loaded);

        function xml_loaded(event:Event):void {
            _xml_config = XML(config_loader.data);
            _config['background'] = _xml_config.children()[0].children();
            _config['items'] = {};
            for each(var xml:XML in _xml_config.children()[1].children()){
                _config['items'][xml.@type] ||= [];
                _config['items'][xml.@type].push({x:xml.@x, y:xml.@y, position:xml.@position, path: xml.children()});
            }
            EventDispatcher.instance.dispatchEvent(new Event(EventDispatcher.CONFIG_LOADED));
        }
    }
    
    public static function get config():Object{
        return _config;
    }
}
}
