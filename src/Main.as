package {

import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.display.DisplayObject;
import flash.display.LoaderInfo;
import flash.display.Sprite;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.text.TextField;
import flash.text.TextFieldAutoSize;
import flash.text.TextFieldType;
import flash.text.TextFormat;

import utils.AssetsLoader;
import utils.EventDispatcher;
import utils.JunkShop;
[SWF(width="720",height="640",frameRate="60",backgroundColor="#002143")]

public class Main extends Sprite {
    private var _for_find: Array;
    private var _for_find_text: TextField;
    
    public function Main() {
        EventDispatcher.instance.addEventListener(EventDispatcher.CONFIG_LOADED, start);
        Config.load();
    }

    private function start(e:Event):void{
        while(numChildren > 1){
            removeChildAt(numChildren - 1);
        }
        create_background();
        create_field();
        draw_types();
    }
    
    protected function create_background():void{
        if (!AssetsLoader.assets['background'])
            (new AssetsLoader).load(Config.config.background, function (e:Event, ...rest):void{ addChildAt((e.target as LoaderInfo).content, 0)});
    }

    protected function create_field():void{
        _for_find = [];
        var items_ary: Array = JunkShop.ary_from_obj_keys(Config.config.items);
        for (var i: int = 0; i < 5; i++){
            var pick:String = items_ary[int(Math.random() * (items_ary.length - 1))];
            while(_for_find.indexOf(pick) >= 0){
                pick = items_ary[int(Math.random() * (items_ary.length - 1))];
            }
            _for_find.push(pick);
        }
        for (var it_key:String in Config.config.items){
            var it:Array = Config.config.items[it_key];
            var picked:Object = it[int(Math.random() * (it.length - 1))];
            (new AssetsLoader).load(picked.path, on_load_image, it_key, picked.position);
        }
    }

    private function on_load_image(e:Event, type_:String, pos: int):void{
        var bmp:Bitmap = new Bitmap(e.target.content.bitmapData);
        var sprite:Sprite = new Sprite();
        sprite.addChild(bmp);
        sprite.name = type_;
        sprite.x = Config.config.items[type_][pos - 1].x;
        sprite.y = Config.config.items[type_][pos - 1].y;
        addChild(sprite);
        sprite.addEventListener(MouseEvent.CLICK, on_find_item);
    }

    private function prepare_text_for_find():void{
        _for_find_text = new TextField();
        _for_find_text.wordWrap = true;
        _for_find_text.multiline = true;
        _for_find_text.width = 200;
        _for_find_text.height = 200;
        _for_find_text.selectable = false;
        _for_find_text.defaultTextFormat = new TextFormat(null, 20, 0xFF00FF);
        addChild(_for_find_text);
    }

    private function draw_types():void{
        if (!(_for_find_text && contains(_for_find_text))){
            prepare_text_for_find();
        }
        _for_find_text.text = '';
        for each(var type_for_find:String in _for_find) {
            _for_find_text.appendText(type_for_find + "\n");
        }
    }

    private function finish_game():void{
        while(numChildren > 1){
            removeChildAt(numChildren - 1);
        }

        _for_find_text = null;

        _for_find = null;

        var finish_text:TextField = new TextField();
        finish_text.autoSize = TextFieldAutoSize.CENTER;
        finish_text.selectable = false;
        finish_text.defaultTextFormat = new TextFormat(null, 30, 0x0000FF);
        finish_text.text = "It's OK!";
        finish_text.x = (stage.width - finish_text.textWidth) / 2;
        finish_text.y = (stage.height - finish_text.textHeight) / 2;
        addChild(finish_text);

        var btn:Sprite = new Sprite();
        btn.graphics.beginFill(0x00FF00);
        btn.graphics.drawRect(0,0,50,20);
        btn.graphics.endFill();
        btn.addEventListener(MouseEvent.CLICK, start);
        btn.x = (stage.width - btn.width) / 2;
        btn.y = finish_text.y + 50;
        addChild(btn);

        var btn_text:TextField = new TextField();
        btn_text.type = TextFieldType.DYNAMIC;
        btn_text.text = 'refresh';
        btn_text.selectable = false;
        btn.addChild(btn_text);
    }

    private function on_find_item(e:MouseEvent):void{
        if (_for_find.indexOf(e.currentTarget.name) >= 0){
            var pixel:uint = (e.currentTarget.getChildAt(0).bitmapData as BitmapData).getPixel32(e.localX, e.localY);
            if (((pixel >> 24) & 0xff) > 0) {
                e.stopImmediatePropagation();
                removeChild(e.currentTarget as DisplayObject);
                _for_find.splice(_for_find.indexOf(e.currentTarget.name), 1);
                draw_types();
                if (_for_find.length == 0){
                    finish_game();
                }
            }
        }
    }
}
}
